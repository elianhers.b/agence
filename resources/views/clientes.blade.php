@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Clientes</div>

                <div class="panel-body">

                    @can('clientes.edit')
                        <p class="text-right">
                            <a href="{{ route('clientes.create') }}" class="btn btn-primary">
                                Crear
                            </a>
                        </p>
                    @endcan

                    <table class="table table-striped">
                        @foreach($clientes as $cliente)
                        <tbody>
                            <tr>
                                <td>{{ $cliente->id }}</td>
                                <td>{{ $cliente->name }}</td>
                                <td>{{ $cliente->description }}</td>

                                @can('clientes.edit')
                                <td>
                                    <a href="{{ route('clientes.edit', $cliente->id) }}" class="btn btn-sm btn-default">
                                        Editar
                                    </a>
                                </td>
                                @endcan

                                @can('clientes.show')
                                <td>
                                    <a href="{{ route('clientes.show', $cliente->id) }}" class="btn btn-sm btn-default">
                                        Ver
                                    </a>
                                </td>
                                @endcan

                                @can('clientes.destroy')
                                <td>
                                    <form action="{{ route('clientes.destroy', $cliente->id) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-sm btn-danger">
                                            Eliminar
                                        </button>
                                    </form>
                                </td>
                                @endcan

                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
