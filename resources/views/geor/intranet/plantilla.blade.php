<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
        <title>
            geor
        </title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/dataTables.min.css') }}" rel="stylesheet">

        <!-- Admin -->
        <link href="{{ asset('admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

        <link href="{{ asset('admin/css/sb-admin-2.min.css') }}" rel="stylesheet">
</head>
<body>

    <div id="app">
      @yield('vue')
    </div>

        <script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
