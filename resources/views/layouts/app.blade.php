<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
 <head>
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
        <title>
            geor
        </title>

        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- carbook -->
        <link rel="stylesheet" href="{{ asset('carbook/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/aos.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/bootstrap-datepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/jquery.timepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/icomoon.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/style.css') }}">

        <!-- Admin -->
        <link href="{{ asset('admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('admin/css/sb-admin-2.css') }}" rel="stylesheet">

    </head>
<body class="bg-body-light">
    <div id="">
        <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled awake" id="ftco-navbar">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                     Geor
                    <span>
                        Concesionarios
                    </span>
                </a>
                <button aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#ftco-nav" data-toggle="collapse" type="button">
                    <span class="oi oi-menu">
                    </span>
                    Menu
                </button>
                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav ml-auto">
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">
                                Iniciar sesión
                            </a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">
                                Registrate
                            </a>
                        </li>
                        @endif
                         @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>


        <main class="py-4">
            @yield('content')
        </main>
    </div>
        <script src="{{ asset('carbook/js/jquery-migrate-3.0.1.min.js') }}"></script>
        <script src="{{ asset('carbook/js/popper.min.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.stellar.min.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('carbook/js/aos.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.animateNumber.min.js') }}"></script>
        <script src="{{ asset('carbook/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.timepicker.min.js') }}"></script>
        <script src="{{ asset('carbook/js/scrollax.min.js') }}"></script>>
        <script src="{{ asset('carbook/js/main.js') }}"></script>
</body>
</html>
