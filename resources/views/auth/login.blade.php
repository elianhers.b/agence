@extends('layouts.app')
@section('content')
<div class="container mt-1">
    <div class="row justify-content-center">
        <div class="col-xl-12 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow mt-5" >
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image" style="min-height: 500px;"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-dark mb-2">
                                        Bienvenidos a GeorConcesionarios!
                                    </h1>
                               </div>
                               <form class="user" method="POST" action="{{ route('login') }}">
                                @csrf
                                   <div class="from-group ">
                                        <input aria-describedby="emailHelp" class="form-control form-control-user @error('email') is-invalid @enderror"  name="email" value="{{ old('email') }}" required id="email" placeholder="Ingrese su correo..." type="email">
                                     @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                                   </div>
                                   <div class="form-group m-2">
                                       <input class="form-control form-control-user @error('password') is-invalid @enderror" id="password" name="password" required placeholder="contraseña" type="password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                   </div>
                                   <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input class="custom-control-input" type="checkbox" name="remember" id="remember"  {{ old('remember') ? 'checked' : '' }} >
                                            <label class="custom-control-label" for="remember">
                                                Recordarme
                                            </label>
                                         </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Iniciar
                                    </button>
                               </form>
                                <hr>
                                <div class="text-center">
                                    @if (Route::has('password.request'))
                                    <a class="small" href="{{ route('password.request') }}">
                                        Olvidaste contraseña?
                                    </a>
                                    @endif
                                </div>
                                <div class="text-center">
                                    <a class="small" href="{{ route('register') }}">
                                        Crear cuenta!
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection
