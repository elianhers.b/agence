<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
        <title>
            geor
        </title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Fonts -->
        <link href="//fonts.gstatic.com" rel="dns-prefetch">
        <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- carbook -->
        <link rel="stylesheet" href="{{ asset('carbook/css/open-iconic-bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/animate.css') }}">

        <link rel="stylesheet" href="{{ asset('carbook/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/magnific-popup.css') }}">

        <link rel="stylesheet" href="{{ asset('carbook/css/aos.css') }}">

        <link rel="stylesheet" href="{{ asset('carbook/css/ionicons.min.css') }}">

        <link rel="stylesheet" href="{{ asset('carbook/css/bootstrap-datepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/jquery.timepicker.css') }}">


        <link rel="stylesheet" href="{{ asset('carbook/css/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/icomoon.css') }}">
        <link rel="stylesheet" href="{{ asset('carbook/css/style.css') }}">

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    Geor
                    <span>
                        Consecionarios
                    </span>
                </a>
                <button aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#ftco-nav" data-toggle="collapse" type="button">
                    <span class="oi oi-menu">
                    </span>
                    Menu
                </button>
                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav ml-auto">
                        @if (Route::has('login'))
                        @auth
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ url('/home') }}">
                                Home
                            </a>
                        </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">
                                Iniciar sesión
                            </a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">
                                Registrate
                            </a>
                        </li>
                        @endif
                        @endauth
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END nav -->
        <div class="hero-wrap ftco-degree-bg" data-stellar-background-ratio="0.5" style="background-image: url( {{ asset( 'carbook/images/bg_1.jpg' )}});">
            <div class="overlay">
            </div>
            <div class="container">
                <div class="row no-gutters slider-text justify-content-start align-items-center justify-content-center">
                    <div class="col-lg-8 ftco-animate">
                        <div class="text w-100 text-center mb-md-5 pb-md-5">
                            <h1 class="mb-4">
                                Manera rápida y fácil de alquilar ó comprar un automóvil
                            </h1>
                            <p style="font-size: 18px;">
                                Geor nace con la finalidad de proporcionarles diversas opciones a la hora de la compra o alquiler de un coche, contando con los mejores financiamientos del mercado.                            </p>
                            <a class="icon-wrap popup-vimeo d-flex align-items-center mt-4 justify-content-center" href="https://www.youtube.com/watch?v=7dyd8GPI84s">
                                <div class="icon d-flex align-items-center justify-content-center">
                                    <span class="ion-ios-play">
                                    </span>
                                </div>
                                <div class="heading-title ml-5">
                                    <span>
                                        Consejos para alquilar o comprar un auto perfecto
                                    </span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="ftco-section ftco-no-pt bg-light">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-12 featured-top">
                        <div class="row no-gutters">
                            <div class="col-md-4 d-flex align-items-center">
                                <form action="#" class="request-form ftco-animate bg-primary">
                                    <h2>
                                        Alquila tu carro
                                    </h2>
                                    <div class="form-group">
                                        <label class="label" for="">
                                            LUGAR DE RECOGIDA
                                        </label>
                                        <input class="form-control" placeholder="City, Airport, Station, etc" type="text">
                                        </input>
                                    </div>
                                    <div class="form-group">
                                        <label class="label" for="">
                                            PUNTO DE ENTREGA
                                        </label>
                                        <input class="form-control" placeholder="City, Airport, Station, etc" type="text">
                                        </input>
                                    </div>
                                    <div class="d-flex">
                                        <div class="form-group mr-2">
                                            <label class="label" for="">
                                                FECHA DE ENTREGA
                                            </label>
                                            <input class="form-control" id="book_pick_date" placeholder="Fecha" type="text">
                                            </input>
                                        </div>
                                        <div class="form-group ml-2">
                                            <label class="label" for="">
                                                FECHA DE DEVOLUCIÓN
                                            </label>
                                            <input class="form-control" id="book_off_date" placeholder="Fecha" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="label" for="">
                                            HORA DE RECOGIDA
                                        </label>
                                        <input class="form-control" id="time_pick" placeholder="Hora" type="text">
                                        </input>
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-secondary py-3 px-4" type="submit" value="Alquila un auto ahora">
                                        </input>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-8 d-flex align-items-center">
                                <div class="services-wrap rounded-right w-100">
                                    <h3 class="heading-section mb-4">
                                        La mejor manera de comprar su auto perfecto
                                    </h3>
                                    <div class="row d-flex mb-4">
                                        <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                                            <div class="services w-100 text-center">
                                                <div class="icon d-flex align-items-center justify-content-center">
                                                    <span class="flaticon-route">
                                                    </span>
                                                </div>
                                                <div class="text w-100">
                                                    <h3 class="heading mb-2">
                                                        Elija su consecionario
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                                            <div class="services w-100 text-center">
                                                <div class="icon d-flex align-items-center justify-content-center">
                                                    <span class="flaticon-handshake">
                                                    </span>
                                                </div>
                                                <div class="text w-100">
                                                    <h3 class="heading mb-2">
                                                        Seleccione la mejor oferta
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                                            <div class="services w-100 text-center">
                                                <div class="icon d-flex align-items-center justify-content-center">
                                                    <span class="flaticon-rent">
                                                    </span>
                                                </div>
                                                <div class="text w-100">
                                                    <h3 class="heading mb-2">
                                                        Reserve su auto
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                        <a class="btn btn-primary py-3 px-4" href="#">
                                            Adquiera su auto perfecto
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-section ftco-no-pt bg-light">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                        <span class="subheading">
                            Alquila o compra tu carro
                        </span>
                        <h2 class="mb-2">
                            Vehículos destacados
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="carousel-car owl-carousel">
                            <div class="item">
                                <div class="car-wrap rounded ftco-animate">
                                    <div class="img rounded d-flex align-items-end" style="background-image: url({{asset('carbook/images/car-1.jpg')}});">
                                    </div>
                                    <div class="text">
                                        <h2 class="mb-0">
                                            <a href="#">
                                                Mercedes Grand Sedan
                                            </a>
                                        </h2>
                                        <div class="d-flex mb-3">
                                            <span class="cat">
                                                Cheverolet
                                            </span>
                                            <p class="price ml-auto">
                                                $500
                                                <span>
                                                    /day
                                                </span>
                                            </p>
                                        </div>
                                        <p class="d-flex mb-0 d-block">
                                            <a class="btn btn-primary py-2 mr-1" href="#">
                                                Reserve ahora
                                            </a>
                                            <a class="btn btn-secondary py-2 ml-1" href="#">
                                                Detalles
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="car-wrap rounded ftco-animate">
                                    <div class="img rounded d-flex align-items-end" style="background-image: url({{ asset( 'carbook/images/car-2.jpg')}});">
                                    </div>
                                    <div class="text">
                                        <h2 class="mb-0">
                                            <a href="#">
                                                Mercedes Grand Sedan
                                            </a>
                                        </h2>
                                        <div class="d-flex mb-3">
                                            <span class="cat">
                                                Cheverolet
                                            </span>
                                            <p class="price ml-auto">
                                                $500
                                                <span>
                                                    /day
                                                </span>
                                            </p>
                                        </div>
                                        <p class="d-flex mb-0 d-block">
                                            <a class="btn btn-primary py-2 mr-1" href="#">
                                                Reserve ahora
                                            </a>
                                            <a class="btn btn-secondary py-2 ml-1" href="#">
                                                Detalles
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="car-wrap rounded ftco-animate">
                                    <div class="img rounded d-flex align-items-end" style="background-image: url({{ asset( 'carbook/images/car-3.jpg')}});">
                                    </div>
                                    <div class="text">
                                        <h2 class="mb-0">
                                            <a href="#">
                                                Mercedes Grand Sedan
                                            </a>
                                        </h2>
                                        <div class="d-flex mb-3">
                                            <span class="cat">
                                                Cheverolet
                                            </span>
                                            <p class="price ml-auto">
                                                $500
                                                <span>
                                                    /day
                                                </span>
                                            </p>
                                        </div>
                                        <p class="d-flex mb-0 d-block">
                                            <a class="btn btn-primary py-2 mr-1" href="#">
                                                Reserve ahora
                                            </a>
                                            <a class="btn btn-secondary py-2 ml-1" href="#">
                                                Detalles
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="car-wrap rounded ftco-animate">
                                    <div class="img rounded d-flex align-items-end" style="background-image: url({{ asset( 'carbook/images/car-4.jpg')}});">
                                    </div>
                                    <div class="text">
                                        <h2 class="mb-0">
                                            <a href="#">
                                                Mercedes Grand Sedan
                                            </a>
                                        </h2>
                                        <div class="d-flex mb-3">
                                            <span class="cat">
                                                Cheverolet
                                            </span>
                                            <p class="price ml-auto">
                                                $500
                                                <span>
                                                    /day
                                                </span>
                                            </p>
                                        </div>
                                        <p class="d-flex mb-0 d-block">
                                            <a class="btn btn-primary py-2 mr-1" href="#">
                                                Reserve ahora
                                            </a>
                                            <a class="btn btn-secondary py-2 ml-1" href="#">
                                                Detalles
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-section ftco-about">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-6 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ asset( 'carbook/images/about.jpg')}});">
                    </div>
                    <div class="col-md-6 wrap-about ftco-animate">
                        <div class="heading-section heading-section-white pl-md-5">
                            <span class="subheading">
                                Sobre nosotros
                            </span>
                            <h2 class="mb-4">
                                Bienvenido a Geor
                            </h2>
                            <p>
                                A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.
                            </p>
                            <p>
                                On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.
                            </p>
                            <p>
                                <a class="btn btn-primary py-3 px-4" href="#">
                                    Sbuscar vehículo
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-7 text-center heading-section ftco-animate">
                        <span class="subheading">
                            servicios
                        </span>
                        <h2 class="mb-3">
                            Nuestros últimos servicios
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="services services-2 w-100 text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-wedding-car">
                                </span>
                            </div>
                            <div class="text w-100">
                                <h3 class="heading mb-2">
                                    Ceremonia de la boda
                                </h3>
                                <p>
                                    A small river named Duden flows by their place and supplies it with the necessary regelialia.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services services-2 w-100 text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-transportation">
                                </span>
                            </div>
                            <div class="text w-100">
                                <h3 class="heading mb-2">
                                    Transferencia de la ciudad
                                </h3>
                                <p>
                                    A small river named Duden flows by their place and supplies it with the necessary regelialia.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services services-2 w-100 text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-car">
                                </span>
                            </div>
                            <div class="text w-100">
                                <h3 class="heading mb-2">
                                    Tasa fija de compra
                                </h3>
                                <p>
                                    A small river named Duden flows by their place and supplies it with the necessary regelialia.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services services-2 w-100 text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-transportation">
                                </span>
                            </div>
                            <div class="text w-100">
                                <h3 class="heading mb-2">
                                    Tour de toda la ciudad
                                </h3>
                                <p>
                                    A small river named Duden flows by their place and supplies it with the necessary regelialia.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-section ftco-intro" style="background-image: url({{ asset( 'carbook/images/bg_3.jpg')}});">
            <div class="overlay">
            </div>
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-md-6 heading-section heading-section-white ftco-animate">
                        <h2 class="mb-3">
                            ¿Quieres ganar con nosotros? Así que no llegues tarde.
                        </h2>
                        <a class="btn btn-primary btn-lg" href="#">
                            Conviertase en un conductor
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-section testimony-section bg-light">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-7 text-center heading-section ftco-animate">
                        <span class="subheading">
                            TESTIMONIAL
                        </span>
                        <h2 class="mb-3">
                            Clientes felices
                        </h2>
                    </div>
                </div>
                <div class="row ftco-animate">
                    <div class="col-md-12">
                        <div class="carousel-testimony owl-carousel ftco-owl">
                            <div class="item">
                                <div class="testimony-wrap rounded text-center py-4 pb-5">
                                    <div class="user-img mb-2" style="background-image: url({{ asset( 'carbook/images/person_1.jpg')}})">
                                    </div>
                                    <div class="text pt-4">
                                        <p class="mb-4">
                                            Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.
                                        </p>
                                        <p class="name">
                                            Roger Scott
                                        </p>
                                        <span class="position">
                                            Gerente de Marketing
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimony-wrap rounded text-center py-4 pb-5">
                                    <div class="user-img mb-2" style="background-image: url({{ asset( 'carbook/images/person_2.jpg')}})">
                                    </div>
                                    <div class="text pt-4">
                                        <p class="mb-4">
                                            Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.
                                        </p>
                                        <p class="name">
                                            Roger Scott
                                        </p>
                                        <span class="position">
                                            Diseñador de interfases
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimony-wrap rounded text-center py-4 pb-5">
                                    <div class="user-img mb-2" style="background-image: url({{ asset( 'carbook/images/person_3.jpg')}})">
                                    </div>
                                    <div class="text pt-4">
                                        <p class="mb-4">
                                            Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.
                                        </p>
                                        <p class="name">
                                            Roger Scott
                                        </p>
                                        <span class="position">
                                            Diseñador UI
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimony-wrap rounded text-center py-4 pb-5">
                                    <div class="user-img mb-2" style="background-image: url({{ asset( 'carbook/images/person_1.jpg')}})">
                                    </div>
                                    <div class="text pt-4">
                                        <p class="mb-4">
                                            Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.
                                        </p>
                                        <p class="name">
                                            Roger Scott
                                        </p>
                                        <span class="position">
                                            Web Developer
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimony-wrap rounded text-center py-4 pb-5">
                                    <div class="user-img mb-2" style="background-image: url({{ asset( 'carbook/images/person_1.jpg')}})">
                                    </div>
                                    <div class="text pt-4">
                                        <p class="mb-4">
                                            Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.
                                        </p>
                                        <p class="name">
                                            Roger Scott
                                        </p>
                                        <span class="position">
                                            Analista de sistemas
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-7 heading-section text-center ftco-animate">
                        <span class="subheading">
                            Blog
                        </span>
                        <h2>
                            Información recientes
                        </h2>
                    </div>
                </div>
                <div class="row d-flex">
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry justify-content-end">
                            <a class="block-20" href="blog-single.html" style="background-image: url({{ asset( 'carbook/images/image_1.jpg')}});">
                            </a>
                            <div class="text pt-4">
                                <div class="meta mb-3">
                                    <div>
                                        <a href="#">
                                            Oct. 29, 2019
                                        </a>
                                    </div>
                                    <div>
                                        <a href="#">
                                            Admin
                                        </a>
                                    </div>
                                    <div>
                                        <a class="meta-chat" href="#">
                                            <span class="icon-chat">
                                            </span>
                                            3
                                        </a>
                                    </div>
                                </div>
                                <h3 class="heading mt-2">
                                    <a href="#">
                                        Why Lead Generation is Key for Business Growth
                                    </a>
                                </h3>
                                <p>
                                    <a class="btn btn-primary" href="#">
                                        Read more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry justify-content-end">
                            <a class="block-20" href="blog-single.html" style="background-image: url({{ asset( 'carbook/images/image_2.jpg')}});">
                            </a>
                            <div class="text pt-4">
                                <div class="meta mb-3">
                                    <div>
                                        <a href="#">
                                            Oct. 29, 2019
                                        </a>
                                    </div>
                                    <div>
                                        <a href="#">
                                            Admin
                                        </a>
                                    </div>
                                    <div>
                                        <a class="meta-chat" href="#">
                                            <span class="icon-chat">
                                            </span>
                                            3
                                        </a>
                                    </div>
                                </div>
                                <h3 class="heading mt-2">
                                    <a href="#">
                                        Why Lead Generation is Key for Business Growth
                                    </a>
                                </h3>
                                <p>
                                    <a class="btn btn-primary" href="#">
                                        Read more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry">
                            <a class="block-20" href="blog-single.html" style="background-image: url({{ asset( 'carbook/images/image_3.jpg')}});">
                            </a>
                            <div class="text pt-4">
                                <div class="meta mb-3">
                                    <div>
                                        <a href="#">
                                            Oct. 29, 2019
                                        </a>
                                    </div>
                                    <div>
                                        <a href="#">
                                            Admin
                                        </a>
                                    </div>
                                    <div>
                                        <a class="meta-chat" href="#">
                                            <span class="icon-chat">
                                            </span>
                                            3
                                        </a>
                                    </div>
                                </div>
                                <h3 class="heading mt-2">
                                    <a href="#">
                                        Why Lead Generation is Key for Business Growth
                                    </a>
                                </h3>
                                <p>
                                    <a class="btn btn-primary" href="#">
                                        Read more
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-counter ftco-section img bg-light" id="section-counter">
            <div class="overlay">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text text-border d-flex align-items-center">
                                <strong class="number" data-number="30">
                                    0
                                </strong>
                                <span>
                                    Años de
                                    <br>
                                        Experiencias
                                    </br>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text text-border d-flex align-items-center">
                                <strong class="number" data-number="15090">
                                    0
                                </strong>
                                <span>
                                    Cantidad de
                                    <br>
                                        Autos
                                    </br>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text text-border d-flex align-items-center">
                                <strong class="number" data-number="1590">
                                    0
                                </strong>
                                <span>
                                    Clientes
                                    <br>
                                        Felices
                                    </br>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text d-flex align-items-center">
                                <strong class="number" data-number="15">
                                    0
                                </strong>
                                <span>
                                    Total
                                    <br>
                                    concesionarios
                                    </br>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="ftco-footer ftco-bg-dark ftco-section">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-md">
                        <div class="ftco-footer-widget mb-4">
                            <h2 class="ftco-heading-2">
                                <a class="logo" href="#">
                                    Conectate
                                    <span>
                                        Con Nosotros
                                    </span>
                                </a>
                            </h2>
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.
                            </p>
                            <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                                <li class="ftco-animate">
                                    <a href="#">
                                        <span class="icon-twitter">
                                        </span>
                                    </a>
                                </li>
                                <li class="ftco-animate">
                                    <a href="#">
                                        <span class="icon-facebook">
                                        </span>
                                    </a>
                                </li>
                                <li class="ftco-animate">
                                    <a href="#">
                                        <span class="icon-instagram">
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="ftco-footer-widget mb-4 ml-md-5">
                            <h2 class="ftco-heading-2">
                                Información
                            </h2>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="py-2 d-block" href="#">
                                       Acerca de
                                    </a>
                                </li>
                                <li>
                                    <a class="py-2 d-block" href="#">
                                        Servicios
                                    </a>
                                </li>
                                <li>
                                    <a class="py-2 d-block" href="#">
                                        Términos y Condiciones
                                    </a>
                                </li>
                                <li>
                                    <a class="py-2 d-block" href="#">
                                       Mejor precio garantizado
                                    </a>
                                </li>
                                <li>
                                    <a class="py-2 d-block" href="#">
                                       Política de privacidad y cookies
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="ftco-footer-widget mb-4">
                            <h2 class="ftco-heading-2">
                                Atención al cliente
                            </h2>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="py-2 d-block" href="#">
                                        Preguntas más frecuentes
                                    </a>
                                </li>
                                <li>
                                    <a class="py-2 d-block" href="#">
                                        Opcion de pago
                                    </a>
                                </li>
                                <li>
                                    <a class="py-2 d-block" href="#">
                                        Consejos de reserva
                                    </a>
                                </li>
                                <li>
                                    <a class="py-2 d-block" href="#">
                                        Cómo funciona
                                    </a>
                                </li>
                                <li>
                                    <a class="py-2 d-block" href="#">
                                        Contáctenos
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="ftco-footer-widget mb-4">
                            <h2 class="ftco-heading-2">
                                ¿Tiene alguna pregunta?
                            </h2>
                            <div class="block-23 mb-3">
                                <ul>
                                    <li>
                                        <span class="icon icon-map-marker">
                                        </span>
                                        <span class="text">
                                            203 Fake St. Mountain View, San Francisco, California, USA
                                        </span>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon icon-phone">
                                            </span>
                                            <span class="text">
                                                +2 392 3929 210
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="icon icon-envelope">
                                            </span>
                                            <span class="text">
                                                info@yourdomain.com
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Geor ©
                            <script>
                                document.write(new Date().getFullYear());
                            </script>
                            Todos los derechos reservados.
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- loader -->
        <div class="show fullscreen" id="ftco-loader">
            <svg class="circular" height="48px" width="48px">
                <circle class="path-bg" cx="24" cy="24" fill="none" r="22" stroke="#eeeeee" stroke-width="4">
                </circle>
                <circle class="path" cx="24" cy="24" fill="none" r="22" stroke="#F96D00" stroke-miterlimit="10" stroke-width="4">
                </circle>
            </svg>
        </div>
        <script src="{{ asset('carbook/js/jquery.min.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery-migrate-3.0.1.min.js') }}"></script>
        <script src="{{ asset('carbook/js/popper.min.js') }}"></script>
        <script src="{{ asset('carbook/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.stellar.min.js') }}"></script>
        <script src="{{ asset('carbook/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('carbook/js/aos.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.animateNumber.min.js') }}"></script>
        <script src="{{ asset('carbook/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('carbook/js/jquery.timepicker.min.js') }}"></script>
        <script src="{{ asset('carbook/js/scrollax.min.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
        <script src="{{ asset('carbook/js/google-map.js') }}"></script>
        <script src="{{ asset('carbook/js/main.js') }}"></script>
    </body>
</html>
