import Vue from 'vue'
import Router from 'vue-router'
import app from './components/intranet/app.vue'
import home from './views/home/home.vue'
import clientes from './views/clientes/clientes.vue'
import resgistrosclientes from './views/auditoria/auditoriaclientes.vue'
import administracionroles from './views/administracion/administracionroles.vue'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path:'/home',	
			name:'home',
			component: home,
		},
		{
			path:'/clientes',
			name:'clientes',
			component: clientes,
		},
		{
			path:'/resgistrosclientes',
			name:'resgistrosclientes',
			component: resgistrosclientes,
		},
		{
			path:'/administracionroles',
			name:'administracionroles',
			component: administracionroles,
		},
	],
	mode: 'history',
})