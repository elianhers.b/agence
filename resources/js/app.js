import router from './routes';

require('./bootstrap');
window.Vue = require('vue');

Vue.component('app', require('./components/intranet/app.vue').default);

//home
Vue.component('home', require('./views/home/home.vue').default);

Vue.component('administracionroles', require('./views/administracion/administracionroles.vue').default);
Vue.component('auditoriaclientes', require('./views/auditoria/auditoriaclientes.vue').default);
Vue.component('clientes', require('./views/clientes/clientes.vue').default);




const app = new Vue({
    el: '#app',
    router,
});
