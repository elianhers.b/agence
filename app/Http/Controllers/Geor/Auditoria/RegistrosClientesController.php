<?php

namespace App\Http\Controllers\Geor\Auditoria;

use App\Auditoria;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RegistrosClientesController extends Controller
{
    public function index()
    {
        return view('geor.intranet.intranet');

    }

    public function getRegistros()
    {
        $auditoria = DB::table('auditorias')
            ->join('users', 'auditorias.user_id', '=', 'users.id')
            ->join('acciones', 'auditorias.acciones_id', '=', 'acciones.id')
            ->select('users.name as u', 'auditorias.*', 'acciones.name as a')
            ->get();
        return $auditoria;
    }

}
