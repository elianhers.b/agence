<?php

namespace App\Http\Controllers\Geor\Administracion;

use App\Http\Controllers\Controller;
use App\Roles;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdministradorRolesController extends Controller
{

    public function index()
    {
        return view('geor.intranet.intranet');
    }

    //trae todos los roles
    public function getRoles()
    {
        $roles = DB::table('roles')
            ->where('statuses_id', 1)
            ->get();
        return $roles;
    }

    //trae todos los permisos
    public function getPermisos()
    {
        return Permission::all();
    }

    //traes todos los usuarios
    public function getUsers()
    {
        return User::all();
    }

    public function store(Request $request)
    {

        $rol = Role::create(['name' => $request->nombreRol, 'statuses_id' => 1]);
        $rol->givePermissionTo($request->give);
        return $result = array('data' => 'Rol creado');
    }

    public function update(Request $request, $id)
    {

    }

    public function eliminarol(Request $request)
    {
        //Eliminar rol
        $rolesBD              = Roles::findOrFail($request->id);
        $rolesBD->statuses_id = 2;
        $rolesBD->save();

        return $result = array('data' => 'Rol eliminado');
    }
    public function asignar(Request $request)
    {
        $userBD           = User::find($request->userbd);
        $userBD->roles_id = $request->rolbd;
        $userBD->save();
        return $result = array('data' => 'Rol Asignado');
    }

}
