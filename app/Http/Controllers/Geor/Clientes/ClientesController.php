<?php

namespace App\Http\Controllers\Geor\Clientes;

use App\Auditoria;
use App\Clientes;
use App\Concesionarios;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClientesController extends Controller
{
    public function index()
    {
        return view('geor.intranet.intranet');

    }

    public function getClients()
    {
        $clientes = DB::table('clientes')
            ->where('statuses_id', 1)
            ->join('concesionarios', 'clientes.concesionarios_id', '=', 'concesionarios.id')
            ->select('clientes.*', 'concesionarios.concesionario')
            ->get();

        return $clientes;
    }

    public function getConcesionarios()
    {
        return Concesionarios::all();
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'newNombre'          => 'required',
            'newApellido'        => 'required',
            'newFechaNacimineto' => 'required',
            'newDni'             => 'required',
            'newEmail'           => 'required',
            'newTelefono'        => 'required',
            'newConcesionario'   => 'required',
        ]);

        //Crear clientes
        $clientes                    = new Clientes;
        $clientes->name              = $request->newNombre;
        $clientes->apellido          = $request->newApellido;
        $clientes->fecha_nacimiento  = $request->newFechaNacimineto;
        $clientes->dni               = $request->newDni;
        $clientes->email             = $request->newEmail;
        $clientes->telefono          = $request->newTelefono;
        $clientes->concesionarios_id = $request->newConcesionario;
        $clientes->statuses_id       = 1;
        $clientes->users_id          = Auth::user()->id;
        $clientes->save();

        //Auditoria
        $atuditoria              = new Auditoria;
        $atuditoria->user_id     = Auth::user()->id;
        $atuditoria->acciones_id = 1;
        $atuditoria->descripcion = 'Se agrego al ususario ' . $clientes->name;
        $atuditoria->save();

        return $result = array('data' => 'Cliente creado');

    }

    public function update(Request $request, $id)
    {

        $clientes                    = Clientes::findOrFail($id);
        $clientes->name              = $request->newNombre;
        $clientes->apellido          = $request->newApellido;
        $clientes->fecha_nacimiento  = $request->newFechaNacimineto;
        $clientes->dni               = $request->newDni;
        $clientes->email             = $request->newEmail;
        $clientes->telefono          = $request->newTelefono;
        $clientes->concesionarios_id = $request->newConcesionario;
        $clientes->save();

        //Auditoria
        $atuditoria              = new Auditoria;
        $atuditoria->user_id     = Auth::user()->id;
        $atuditoria->acciones_id = 3;
        $atuditoria->descripcion = 'Se edito al ususario ' . $clientes->name;
        $atuditoria->save();

        return $result = array('data' => 'Cliente editado');
    }

    public function destroy($id)
    {
        //Eliminar clientes
        $clientes              = Clientes::findOrFail($id);
        $clientes->statuses_id = 2;
        $clientes->save();

        //Auditoria
        $atuditoria              = new Auditoria;
        $atuditoria->user_id     = Auth::user()->id;
        $atuditoria->acciones_id = 2;
        $atuditoria->descripcion = 'Se elimino al ususario ' . $clientes->name;
        $atuditoria->save();

        return $result = array('data' => 'Cliente eliminado');
    }

}
