<?php

namespace App\Http\Controllers;

use App\Clientes;

class ClientesController extends Controller
{
    public function index()
    {
        $clientes = Clientes::get();

        return view('clientes', compact('clientes'));
    }

    public function create()
    {
        return 'Tiene permiso de crear';
    }

    public function show(Clientes $clientes)
    {
        return 'Tiene permiso de ver';
    }

    public function edit(Clientes $clientes)
    {
        return 'Tiene permiso de editar';
    }

    public function destroy(Clientes $clientes)
    {
        return 'Tiene permiso de eliminar';
    }
}
