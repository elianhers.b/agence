<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('geor.intranet.intranet');
    }

    public function getUserAuth()
    {
        $result = array(
            '0' => Auth::user(),
        );

        return $result;

    }

    public function getPermisos()
    {
        $user     = Auth::user()->id;
        $permisos = DB::table('users')
            ->where('users.id', $user)
            ->join('roles', 'users.roles_id', '=', 'roles.id')
            ->join('role_has_permissions', 'roles.id', '=', 'role_has_permissions.role_id')
            ->join('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
            ->select('permissions.*')
            ->get();

        return $permisos;

    }

}
