<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = ['name', 'apellido', 'fecha_nacimiento', 'dni', 'email', 'telefono', 'concesionarios_id', 'statuses_id', 'users_id'];
}
