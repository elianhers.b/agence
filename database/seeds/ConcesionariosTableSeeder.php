<?php

use Illuminate\Database\Seeder;

class ConcesionariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Concesionarios::create(['concesionario' => 'Chiclayo']);
        App\Concesionarios::create(['concesionario' => 'Lima']);
        App\Concesionarios::create(['concesionario' => 'Trujillo']);
        App\Concesionarios::create(['concesionario' => 'Cusco']);
        App\Concesionarios::create(['concesionario' => 'Iquitos']);
        App\Concesionarios::create(['concesionario' => 'Tacna']);
        App\Concesionarios::create(['concesionario' => 'Piura']);
        App\Concesionarios::create(['concesionario' => 'Arequipa']);
        App\Concesionarios::create(['concesionario' => 'Huancayo']);
        App\Concesionarios::create(['concesionario' => 'ica']);
        App\Concesionarios::create(['concesionario' => 'Ayacucho']);
        App\Concesionarios::create(['concesionario' => 'Chimbote']);
        App\Concesionarios::create(['concesionario' => 'Tumbes']);
        App\Concesionarios::create(['concesionario' => 'Pullcapa']);
        App\Concesionarios::create(['concesionario' => 'Callao']);
    }
}
