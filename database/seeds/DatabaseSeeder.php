<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StatusTableSedder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ConcesionariosTableSeeder::class);
        $this->call(ClientesSeeder::class);
        $this->call(AccionesTableSeeder::class);
    }
}
