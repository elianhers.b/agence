<?php

use Illuminate\Database\Seeder;

class AccionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Acciones::create(['name' => 'Adición']);
        App\Acciones::create(['name' => 'Eliminación']);
        App\Acciones::create(['name' => 'Edición']);
    }
}
