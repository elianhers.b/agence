<?php

use Illuminate\Database\Seeder;

class StatusTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Statuses::create(['status' => 'Activo']);
        App\Statuses::create(['status' => 'Inactivo']);
    }
}
