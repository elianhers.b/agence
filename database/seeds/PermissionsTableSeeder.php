<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Permission list
        Permission::create(['name' => 'modulo.clientes']);
        Permission::create(['name' => 'editar.clientes']);
        Permission::create(['name' => 'mostrar.clientes']);
        Permission::create(['name' => 'crear.clientes']);
        Permission::create(['name' => 'eliminar.clientes']);
        Permission::create(['name' => 'modulo.roles']);
        Permission::create(['name' => 'editar.roles']);
        Permission::create(['name' => 'mostrar.roles']);
        Permission::create(['name' => 'crear.roles']);
        Permission::create(['name' => 'eliminar.roles']);
        Permission::create(['name' => 'modulo.auditoria']);
        //Admin
        $admin = Role::create(['name' => 'Admin', 'statuses_id' => 1]);

        $admin->givePermissionTo([
            'modulo.clientes',
            'editar.clientes',
            'mostrar.clientes',
            'crear.clientes',
            'eliminar.clientes',
            'modulo.roles',
            'editar.roles',
            'mostrar.roles',
            'crear.roles',
            'eliminar.roles',
            'modulo.auditoria',
        ]);

        //user_simple
        $user_simple = Role::create(['name' => 'user_simple', 'statuses_id' => 1]);

        $user_simple->givePermissionTo([
            'modulo.clientes',
            'mostrar.clientes',
        ]);

        App\User::create([
            'name'     => 'Elianhers blanco',
            'email'    => 'elianhers.b@gmail.com',
            'password' => bcrypt('123456'),
            'roles_id' => 1,

        ]);

        //User Admin
        $user = User::find(1);
        $user->assignRole('Admin');
    }
}
