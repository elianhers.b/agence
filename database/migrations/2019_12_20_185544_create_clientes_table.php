<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concesionarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('concesionario');
            $table->timestamps();
        });

        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->text('apellido');
            $table->date('fecha_nacimiento');
            $table->integer('dni');
            $table->text('email');
            $table->text('telefono');
            $table->integer('concesionarios_id');
            $table->foreign('concesionarios_id')
                ->references('id')
                ->on('concesionarios');
            $table->integer('statuses_id');
            $table->foreign('statuses_id')
                ->references('id')
                ->on('statuses');
            $table->integer('users_id');
            $table->foreign('users_id')
                ->references('id')
                ->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concensionarios');

        Schema::dropIfExists('clientes');
    }
}
