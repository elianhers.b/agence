<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Clientes;
use Faker\Generator as Faker;

$factory->define(Clientes::class, function (Faker $faker) {

    return [
        'name'              => $faker->name,
        'apellido'          => $faker->name,
        'fecha_nacimiento'  => $faker->date('Y-m-d'),
        'dni'               => 22222,
        'email'             => $faker->unique()->safeEmail,
        'telefono'          => $faker->phoneNumber,
        'concesionarios_id' => rand(1, 15),
        'statuses_id'       => 1,
        'users_id'          => 1,
    ];
});
