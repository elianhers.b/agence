<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/user', 'HomeController@getUserAuth')->name('user');
Route::get('/permisos', 'HomeController@getPermisos')->name('permisos');

Route::middleware(['auth'])->group(function () {

    Route::get('/getclients', 'Geor\Clientes\ClientesController@getClients')->name('getClients');

    Route::get('/getroles', 'Geor\Administracion\AdministradorRolesController@getRoles')->name('getroles');
    Route::get('/getpermisos', 'Geor\Administracion\AdministradorRolesController@getPermisos')->name('getpermisos');
    Route::get('/getusers', 'Geor\Administracion\AdministradorRolesController@getUsers')->name('getusers');

    Route::get('/getconcesionarios', 'Geor\Clientes\ClientesController@getConcesionarios')->name('getConcesionarios');

    Route::get('getresgistrosclientes', 'Geor\Auditoria\RegistrosClientesController@getRegistros')->name('resgistrosclientes.index');

    Route::get('/administracionroles', 'Geor\Administracion\AdministradorRolesController@index')->name('administracionroles.index');

    Route::resource('clientes', 'Geor\Clientes\ClientesController', ['except' => 'show', 'create', 'edit']);

    Route::resource('administracionroles', 'Geor\Administracion\AdministradorRolesController', ['except' => 'show', 'create', 'edit']);
    Route::post('eliminarol', 'Geor\Administracion\AdministradorRolesController@eliminarol')->name('eliminarol');
    Route::post('asignarol', 'Geor\Administracion\AdministradorRolesController@asignar')->name('asignarol');
});
